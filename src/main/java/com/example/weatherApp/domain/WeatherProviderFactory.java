package com.example.weatherApp.domain;

public class WeatherProviderFactory {

    public static WeatherProvider createWeatherProvider(String providerName) {
        if (providerName.equalsIgnoreCase("ApiProvider")) {
            return new ApiWeatherProvider();
        } else if (providerName.equalsIgnoreCase("Database")) {
            return new DataBaseProvider();
        } else {
            throw new IllegalArgumentException("Unknown weather provider: " + providerName);
        }
    }
}
