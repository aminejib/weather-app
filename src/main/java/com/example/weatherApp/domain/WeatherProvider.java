package com.example.weatherApp.domain;

public interface WeatherProvider {
    String getWeatherData(String location);
}
