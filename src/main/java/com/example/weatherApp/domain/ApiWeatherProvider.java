package com.example.weatherApp.domain;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

public class ApiWeatherProvider implements WeatherProvider{

    @Value("${api.url}")
    private  String apiUrl;

    @Value("${api.key}")
    private String apiKey;

    @Override
    public String getWeatherData(String location) {

        RestTemplate restTemplate = new RestTemplate();
        String url = apiUrl + "?q=" + location + "&appid=" + apiKey;
        return restTemplate.getForObject(url, String.class);
    }
}
